import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization:
      'Client-ID jK8ZkujVEdoVYoQJ507gVuxkVEol2oY9CpRyxyXx8jI'
  }
});
